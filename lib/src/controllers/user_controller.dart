import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../generated/l10n.dart';
import '../helpers/helper.dart';
import '../models/user.dart';
import '../repository/user_repository.dart' as repository;
import '../WidgetLoading.dart';

class UserController extends ControllerMVC {
	User user = new User();
	bool hidePassword = true;
	bool loading = false;
	GlobalKey<FormState> loginFormKey;
	GlobalKey<ScaffoldState> scaffoldKey;
	FirebaseMessaging _firebaseMessaging;
	OverlayEntry _loader;
	OverlayEntry get loader
	{
		if( this._loader == null )
			this._loader = Helper.overlayLoader(this.context);
			
		return this._loader;
	}
	BuildContext get context
	{
		return this.state.context;
	}
	
	UserController() 
	{
		//loader = Helper.overlayLoader(this.context);
		this.loginFormKey = new GlobalKey<FormState>();
		this.scaffoldKey = new GlobalKey<ScaffoldState>();
		this._firebaseMessaging = FirebaseMessaging();
		print('FIREBASE: Getting token');
		this._firebaseMessaging.getToken().then((String _deviceToken) 
		{
			user.deviceToken = _deviceToken;
			print('FIREBASE: Device token -> ${_deviceToken}');
		}).catchError((e) {
		  print('FIREBASE: Notification not configured');
		});
	}

	void login() async 
	{
		FocusScope.of(context).unfocus();
		if ( !this.loginFormKey.currentState.validate() ) 
			return;

		this.loginFormKey.currentState.save();
		Overlay.of(context).insert(loader);
		try
		{
			var value = await repository.login(user);
			if (value != null && value.apiToken != null) 
			{
				Navigator.of(scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 2);
			} 
			else 
			{
				scaffoldKey?.currentState?.showSnackBar(SnackBar(
					content: Text(S.of(context).wrong_email_or_password),
				));
			}
			Helper.hideLoader(loader);
		}
		catch(e)
		{
			loader.remove();
			Helper.hideLoader(loader);
			scaffoldKey?.currentState?.showSnackBar(SnackBar(
			  content: Text(S.of(context).this_account_not_exist),
			));
		}
	}

  void register() async {
    FocusScope.of(context).unfocus();
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      Overlay.of(context).insert(loader);
      repository.register(user).then((value) {
        if (value != null && value.apiToken != null) {
          Navigator.of(scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 2);
        } else {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.of(context).wrong_email_or_password),
          ));
        }
      }).catchError((e) {
        loader.remove();
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(S.of(context).this_email_account_exists),
        ));
      }).whenComplete(() {
        Helper.hideLoader(loader);
      });
    }
  }

  void resetPassword() {
    FocusScope.of(context).unfocus();
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      Overlay.of(context).insert(loader);
      repository.resetPassword(user).then((value) {
        if (value != null && value == true) {
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.of(context).your_reset_link_has_been_sent_to_your_email),
            action: SnackBarAction(
              label: S.of(context).login,
              onPressed: () {
                Navigator.of(scaffoldKey.currentContext).pushReplacementNamed('/Login');
              },
            ),
            duration: Duration(seconds: 10),
          ));
        } else {
          loader.remove();
          scaffoldKey?.currentState?.showSnackBar(SnackBar(
            content: Text(S.of(context).error_verify_email_settings),
          ));
        }
      }).whenComplete(() {
        Helper.hideLoader(loader);
      });
    }
  }
	void facebookLogin() async
	{
		final facebookLogin = FacebookLogin();
		facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;//nativeWithFallback;
		final result = await facebookLogin.logIn(['email']);

		switch (result.status) 
		{
			case FacebookLoginStatus.loggedIn:
				String fb_token = result.accessToken.token;
				//##get facebook user data
				final graphResponse = await http.get(
					Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${fb_token}')
				);
				final profile = json.decode(graphResponse.body);
				//##get user avatar
				final avatarRes = await http.get( Uri.parse('https://graph.facebook.com/'+ profile['id'] +'/picture?type=large&redirect=false&access_token=' + fb_token) );
				final avatar = json.decode(avatarRes.body);
				//print(avatar);
				var data = {
					'fb_id': 	profile['id'],
					'email': 	profile['email'] != null ? profile['email'] : null,
					'name': 	profile['name'],
					'avatar': 	avatar['data']['url'],
					'token': 	fb_token,
					'device_token': this.user.deviceToken,
				};
				print(data);
				//{name: Juan Marcelo Aviles Paco, first_name: Juan Marcelo, last_name: Aviles Paco, email: marce_nickya@yahoo.es, id: 2982572738452325}
				var _key = GlobalKey<State>();
				WidgetLoading.show(context, _key, 'Validando información');
				repository.fbLogin(data)
					.then( (user) 
					{
						WidgetLoading.hide(_key);
						Navigator.pop(context);
						Navigator.of(this.scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 2);
					})
					.catchError( (e) 
					{
						WidgetLoading.hide(_key);
						print('FB LOGIN ERROR: ');
						print(e);
						var alert = AlertDialog(
							title: Text("Error en inicio de sesión"),
							content: Text("Ocurrio un error al inciar la sesión, intentelo más adelante."),
							actions: <Widget>[
								FlatButton(
									child: Text("Cerrar"), 
									onPressed: ()
									{
										Navigator.of(context).pop();
									}
								)
							]
						);
						showDialog(context: context, builder: (BuildContext ctx){return alert;});
					});
				
		    break;
		  	case FacebookLoginStatus.cancelledByUser:
		    break;
		  	case FacebookLoginStatus.error:
				print('ERROR: ' + result.errorMessage);
		    break;
		}
	}
	void googleLogin() async
	{
		try
		{
			GoogleSignIn gsi = GoogleSignIn(scopes:['email', 'https://www.googleapis.com/auth/contacts.readonly']);
			gsi.onCurrentUserChanged.listen( (GoogleSignInAccount account) 
			{
				print('onCurrentUserChanged');
				var data = {
					'g_id': 	account.id,
					'email': 	account.email != null ? account.email : null,
					'name': 	account.displayName,
					'avatar':	account.photoUrl,
					'device_token': this.user.deviceToken,
				};
				
				if( account != null )
				{
					account.authHeaders.then( (h) 
					{
						//print(h);
						data['token'] = h['Authorization'].toString().replaceAll('Bearer', '').trim();
						print(data);
						final GlobalKey<State> _key = GlobalKey<State>();
						WidgetLoading.show(context, _key, 'Verificando información...');
						//##send data to server
						repository.googleLogin(data)
							.then( (user) 
							{
								WidgetLoading.hide(_key);
								Navigator.pop(context);
								Navigator.of(this.scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 2);
							})
							.catchError( (e) 
							{
								print(e);
								WidgetLoading.hide(_key);
								var alert = AlertDialog(
									title: Text("Error en inicio de sesión"),
									content: Text("Ocurrio un error al inciar la sesión, intentelo más adelante."),
									actions: <Widget>[
										FlatButton(
											child: Text("Cerrar"), 
											onPressed: ()
											{
												Navigator.of(context).pop();
											}
										)
									]
								);
								showDialog(context: context, builder: (BuildContext ctx){return alert;});
							});
					});
				}
			});
			gsi.signInSilently();
			await gsi.signIn();
		}
		catch(error)
		{
			print(error);
		}
	}
	void appleLogin() async
	{
		try
		{
			final credentials = await SignInWithApple.getAppleIDCredential(
				scopes: [AppleIDAuthorizationScopes.email, AppleIDAuthorizationScopes.fullName]
			);
			var data = {
				'apple_id': credentials.userIdentifier,
				'email': 	credentials.email,
				'name': '${credentials.givenName} ${credentials.familyName}',
				'avatar':	null,
				'token': 	credentials.identityToken,
				'device_token': this.user.deviceToken,
			};
			var _key = GlobalKey<State>();
			WidgetLoading.show(context, _key, 'Validando información...');
			repository.appleLogin(data).then( (user) 
			{
				WidgetLoading.hide(_key);
				Navigator.pop(context);
				Navigator.of(this.scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 2);
			})
			.catchError((e)
			{
				WidgetLoading.hide(_key);
				print(e);
			});
		}
		catch(e)
		{
			print('ERROR');
			print(e);
		}
	}
}
