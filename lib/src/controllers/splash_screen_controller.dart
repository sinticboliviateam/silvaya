import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../helpers/custom_trace.dart';
import '../repository/settings_repository.dart' as settingRepo;
import '../repository/user_repository.dart' as userRepo;

Future<void> onBackgroundMessage(RemoteMessage message) async 
{
	await Firebase.initializeApp();
	print('Handling a background message ${message.messageId}');
	/*
	Fluttertoast.showToast(
		msg: message.message,
		toastLength: Toast.LENGTH_LONG,
		gravity: ToastGravity.TOP,
		timeInSecForIosWeb: 5,
	);
	*/
}
class SplashScreenController extends ControllerMVC 
{
	ValueNotifier<Map<String, double>> progress = new ValueNotifier(new Map());
	GlobalKey<ScaffoldState> scaffoldKey;
	final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
	//var context;
	
	BuildContext get context
	{
		return this.state.context;
	}
	SplashScreenController() 
	{
		//this.context = this.state.context;
		this.scaffoldKey = new GlobalKey<ScaffoldState>();
		// Should define these variables before the app loaded
		progress.value = {"Setting": 0, "User": 0};
	}

	@override
	void initState() 
	{
	    super.initState();
	    this.configureFirebase();
	    settingRepo.setting.addListener(() {
			print('Notifying settings loaded');
			if (settingRepo.setting.value.appName != null && settingRepo.setting.value.appName != '' && settingRepo.setting.value.mainColor != null) {
				progress.value["Setting"] = 41;
				// ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
				progress?.notifyListeners();
			}
	    });
	    userRepo.currentUser.addListener(() 
		{
			print('Notifying user loaded');
			print('${userRepo.currentUser}');
			if (userRepo.currentUser.value.auth != null) {
				progress.value["User"] = 59;
				progress?.notifyListeners();
			}
	    });
	    Timer(Duration(seconds: 20), () {
	      scaffoldKey?.currentState?.showSnackBar(SnackBar(
	        content: Text(S.of(context).verify_your_internet_connection),
	      ));
	    });
	}

	void configureFirebase() 
	{
		print('FIREBASE: Configuring');
		
		this.firebaseMessaging.requestNotificationPermissions(const IosNotificationSettings(sound: true, badge: true, alert: true));
		try 
		{
			
			/*
			this.firebaseMessaging.configure(
				onMessage: notificationOnMessage,
				onLaunch: notificationOnLaunch,
				onResume: notificationOnResume,
			);
			*/
			FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);
			FirebaseMessaging.onMessage.listen( this.notificationOnMessage );
			FirebaseMessaging.instance
			.getToken( 
				vapidKey: 'BGpdLRsMJKvFDD9odfPk92uBg-JbQbyoiZdah0XlUyrjG4SDgUsE1iC_kdRgt4Kn0CO7K3RTswPZt61NNuO0XoA'
			).then((token)
			{
				print('Firebase token: $token');
			});
			
		} catch (e) {
		  print(CustomTrace(StackTrace.current, message: e));
		  print(CustomTrace(StackTrace.current, message: 'FIREBASE: Error Config Firebase'));
		}
	}

  Future notificationOnResume(Map<String, dynamic> message) async {
    print(CustomTrace(StackTrace.current, message: message['data']['id']));
    try {
      if (message['data']['id'] == "orders") {
        settingRepo.navigatorKey.currentState.pushReplacementNamed('/Pages', arguments: 3);
      }
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

  Future notificationOnLaunch(Map<String, dynamic> message) async {
    String messageId = await settingRepo.getMessageId();
    try {
      if (messageId != message['google.message_id']) {
        if (message['data']['id'] == "orders") {
          await settingRepo.saveMessageId(message['google.message_id']);
          settingRepo.navigatorKey.currentState.pushReplacementNamed('/Pages', arguments: 3);
        }
      }
    } catch (e) {
      print(CustomTrace(StackTrace.current, message: e));
    }
  }

	//Future<void> notificationOnMessage(Map<String, dynamic> message) async 
	void notificationOnMessage(RemoteMessage message) async 
	{
		Fluttertoast.showToast(
			//msg: message['notification']['title'],
			msg: message.notification.title,
			toastLength: Toast.LENGTH_LONG,
			gravity: ToastGravity.TOP,
			timeInSecForIosWeb: 5,
		);
	}
}
